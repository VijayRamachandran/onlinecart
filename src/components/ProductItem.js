import React from 'react'
import PropTypes from 'prop-types'
import Product from './Product'

const ProductItem = ({ product, onAddToCartClicked }) => (
  <div style={{ marginBottom: 20 }}>
    <Product
      title={product._source.title}
      price={product._source.giftprice}
       />
    <button
      onClick={onAddToCartClicked}
      disabled={product._source.donationscount > 0 ? '' : ''}>
      {product._source.donationscount > 0 ? 'Add to cart' : 'Add to cart'}
    </button>
  </div>
)

ProductItem.propTypes = {
  product: PropTypes.shape({
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    inventory: PropTypes.number.isRequired,
	datepublished:PropTypes.string.isRequired,
  }).isRequired,
  onAddToCartClicked: PropTypes.func.isRequired
}

export default ProductItem
